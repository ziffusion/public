# Structuring and organizing code #
What is clear is that structuring code is important. What is often not clear is what good structure is. As we code, we need to make decisions about how to decompose functionality, how to determine layering, how to abstract classes. Is it better to do this or that? Why is one better or worse than the other? Why are gotos bad? Why are globals bad? Can class variables be as bad as globals? Sometimes there isn't a clear answer. Some decisions seem arbitrary and capricious.

Plenty of programming paradigms for organizing code exist - object oriented, functional, data flow oriented, rule based, and so on. It would be presumptuous to think that we can identify the right answer, when smarter people are still struggling with it. But it may be possible to articulate some principles that can help guide our hands while writing code.
## What is good structure? ##
What goes unsaid (or at least not clearly enough) is the simple truth about what it is all about. Why do we fuss so much over good structure, good design, and good architecture? In the final analysis it is simply about human cognition. We need to be able to comprehend, and reason about large code bases. If we can reason clearly about the code, we can decrease bugs, write correct and robust logic, add functionality easier (without breaking other things), and refactor to a different architecture that allows us to do more things better.

Any principle of organizing code that helps with human cognition is good.
## But what is it really? ##
What is really going on when we organize code? Code is simply our thought process, written down. A thought process that captures all the functionality in the system, with all of the million details that the functionality comprises. It is information. The way we humans deal with enormous amounts of information is by compartmentalizing and organizing it in hierarchical structures.

Programmers are in the business of organizing information. They become experts at organizing complexity in ways that keeps it tractable.

But how does compartmentalization and hierarchical structure help with cognition? Recognizing certain key aspects of it can help us make better decisions in structuring code.
### Principle of reduction of surface area ###
Any object or layer that we abstract is basically an act of delegating a bunch of details to this object or layer. In delegating these details to the object, we ask it to deal with them, so we don't have to ourselves. Instead, we deal with the interface that the object offers. This is an act of reduction of surface area of information. We need to know a lot less. We need to hold a lot less in our heads while reasoning at this level of abstraction.

Therefore, in reducing surface area, we constantly ask ourselves - do I need to expose this detail outside of this object? If a detail is leaking through unnecessarily, then it may be a leaky abstraction. How do I design this abstraction to minimize the surface area?

The efficiency in terms of reduction of surface area is proportional to how much information an object is able to hide within itself.
### Principle of hierarchical organization ###
This process of reduction of surface area can be applied recursively, where chunks of functionality (details, information) gets delegated recursively to a hierarchy of objects. An object may offer an interface with a reduced surface area, but it may in turn delegate aspects of it's own functionality to other lower level objects. The idea is to keep the surface area tractable at each level of abstraction.

It's a skill to think in terms of these hierarchy of abstractions, where the surface area at each level is manageable, and can be reasoned about. Thinking in terms of this structure is what allows us to comprehend an entire system, without getting hopelessly lost in the details.

It's a skill to consciously keep the abstractions distinct, think in terms of their interfaces, and move from one to other without mixing up their internal details.

Of course, at times, we have to travel up and down this hierarchy, and think about the details at each level. The trick is to compartmentalize, and deal with each level of abstraction separately.
### Principle of meaningful abstraction ###
This principle flows from the underlying reason for why we want to structure information in the first place. We are trying to facilitate human cognition. And humans deal with meaningful abstractions better than a random clutter of information.

It helps if an object or a method implements an abstraction that can be described succinctly. What is this object, and what does it do for me? What is it's programming model? How do I program to its interface to invoke it's services? If there isn't a short clear answer to these questions, then it may not be a good abstraction. This is why people argue over if the abstraction is concise enough, or if the interface makes sense. We are simply talking about if the abstraction is meaningful to the human thought process.

We want to offer a usable interface for an object, an interface that is easy to think about and program to. Different syntactic elements play crucial roles. Naming is the primary mechanism for communicating the meaning of an abstraction. The public interface captures the programming model. Method signatures capture the information flowing across the interface.

We want to go beyond throwing together a hodgepodge of objects and methods to get something done. We want to build meaningful abstractions, where objects and methods have comprehensible stories.
## Summary ##
Viewing coding as organizing information is an interesting perspective. Creating structure, recognizing it, and navigating it - are fundamental skills. Moving from abstraction to abstraction, thinking in terms of interfaces, confining ourselves to the surface area at a certain level is a fundamental skill. It's a way to deal with complexity.